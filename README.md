# GitLab AI Experimentation Plugin


**Please note this extension ONLY works on https://gitlab.com** 

This repository contains a Chrome extension that can be built upon for rapidly testing out new prototype(s) and ideas for utilizing AI in various GitLab Features.

This Plugin has two modes of operation:
1. Framework allowing developers and anyone with Vue and some JavaScript experience to build out new experiences by dynamically adding it to gitlab.com web pages.
2. Provides a web view to a collection of prompts. This overlay can read data from the current page to send it to OpenAI with various prompts that are stored in the [prompt collection repository](https://gitlab.com/gitlab-org/dev-subdepartment/ai-dev-promptcollection).
 
## Building

First, install the necessary packages with `yarn install` to get started.

If you just want to build the extension, then after yarn install, run `yarn build`. If you want to develop while using the extension, use `yarn serve`. Note changes are not _always_ reflected and in many cases the plugin has to be forcibly reloaded, or even re-installed. See troubleshooting for more information.

## Initialization

### Initialization Steps:

1. To initialize the plugin you will need to run the build process. After it is built you will need to go to `chrome://extensions/` in your browser and enable Developer mode in the top right corner:

![](docs/developer_mode.png){width=50%}

2. You must now click the `Load Unpacked`, navigate to the location of this repositories `./dist` directory which should exist after building the project. 

![](docs/load_unpacked.png){width=50%}

3. On success you should now see the "GitLab AI Experimentation Chrome Plugin" in your list of enabled plugins:

![](docs/gitlab_plugin.png){width=50%}

4. Next, you must configure the plugin. In the top right corner of your browser you should see a puzzle icon, click that to pin the GitLab extension, as well as click on it to open the settings panel:

![](docs/puzzle_icon.png){width=50%}

1. Now click the pin icon to pin the icon to always show up in your browser extensions list:

![](docs/pin_extension.png){width=25%}

6. Once pinned, you can now click the GitLab Tanuki to bring up the settings panel

![](docs/settings_panel.png){width=50%}


7. Enter your OpenAI API Key and click "Save".
8. You may need to re-open the settings panel to chose a proper OpenAI Model (it requires the key bet set before we can load the list of available models).
9. To show the `Prompt Collection Overlay` click the `Show AI Assist Button` and click `Save`
10. You should now see an AI Assist button next to your profile picture when you visit https://gitlab.com.

Note: OpenAI Keys are only temporarily saved, if you close your browser, or reload the extension, the API key will be removed.

### Rapid Prototyping Development Framework

This section is for developers, engineers or interested folks who want to build their own prototype by inserting features directly into gitlab.com web pages.

Please note this feature is **highly experimental**,  injecting Vue components into a page dynamically is not really how Vue was meant to be used. That being said this can be a very quick method of testing out an idea before running changes on the monolith or creating MRs against the gitlab repository. It is very much meant for **prototyping ONLY**.

To add a new feature, first you must determine where and on which pages this feature will exist. Let's use the MRDiff feature as an example.

We would like to inject a button that says `Explain` into every MR file diff Card. When clicked it will bring up the files (diff) details with a button saying "Explain" which the user can click to query an AI model to return an analysis of the changes.


To determine where and when we will inject this component we need some information:
1. The page type we want to inject into
2. Which locations in the web page we want to inject our new component
3. The name of our component


### Page type
Looking at the `projects/-/merge_requests/<MR>/diffs` source we notice a few things when inspecting the HTML body from devtools.

```html
<body class="gl-dark tab-width-8 gl-browser-chrome gl-platform-mac page-initialised" data-find-file="/XXX/sqlite/-/find_file/main" data-namespace-id="XXX" data-page="projects:merge_requests:show" data-page-type-id="1" data-project="sqlite" data-project-id="XXX" gl-extension-id="XXX">
...

```

First is the attribute `data-page="projects:merge_requests:show"`. We want to inject our button on this `data-page` type, we can use just the start of this string: `projects:merge_requests` as the page type(s) we want to inject our feature into. 

We now have our page type, which is `projects:merge_requests`

### Locations in the page to inject our component

For the MR Diff it makes sense to inject a button on each file view card in the diff view. Looking at the source we find the following HTML element:

```html
<div class="file-header-content"><button aria-label="Hide file contents" type="button" class="btn gl-mr-2 btn-default btn-sm gl-button btn-default-tertiary btn-icon"><!----> <svg data-testid="chevron-down-icon" role="img" aria-hidden="true" class="gl-button-icon gl-icon s16"><use href="/assets/icons-87cb0ce1047e0d3e1ddd352a88d6807e6155673ebba21022180ab5ee153c2026.svg#chevron-down"></use></svg>  <!----></button> <a v-once="true" href="#diff-content-8fac9a47149b36c124ca58d360a6f75158b79b3e" class="gl-mr-2 gl-text-decoration-none! gl-word-break-all"><strong title="src/backup.c" data-container="body" data-qa-selector="file_name_content" class="file-title-name">
        src/backup.c
```

The `file-header-content` div looks like a good candidate to chose as our location. We will chose this by coming up with a CSS selector that will work with [document.querySelectorAll](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll). In our case the css selector `div[class="file-header-content"]` will find all divs with this class applied. Meaning we will inject our button in the element [adjacent to it.](https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentElement). 

We now have our location information.

### Name of the component

This is the name of the vue component you will create and can be named anything but it should not conflict with other possible components. Choose something unique like: `gl-ai-extension-{feature}-app`.

We now have all information we need, let's create our component:

1. Create a new directory of {feature name} in the `src` directory, in our case `./mrdiff/`.
2. Create a `main.js` and paste in the following information, note the appName variable:
```js
import Vue from 'vue';
import App from './App.vue';

import setConfigs from '@gitlab/ui/dist/config';

setConfigs();

const appName = 'gl-ai-extension-mrdiff-app'; // make sure this is your appName!

/* eslint-disable no-new */
new Vue({
  el: 'div[class="'+appName+'"]',
  appName: appName,
  render: (h) => h(App),
});
```

3. Next create the [./src/mrdiff/App.vue](./src/mrdiff/App.vue)
4. Now create a `{feature}.js` file in the `./src/content-scripts/` directory, in our case `mrdiff.js`. Note that the `appJS` name is created by webpack by taking `./src/{feature}/main.js` and building it to the `dist/js/{feature}.js`
```javascript
import { CreateContainer, InjectComponent } from "./injector";

export function MRDiff(extensionId) {
    const appName = 'gl-ai-extension-mrdiff-app'; // Note the name of the appName is the same as the App.vue appName!
    const appJS = '/js/mrdiff.js' // This is the name of your feature(s) directory (e.g. src/mrdiff/main.js -> /js/mrdiff.js).
    const pageType = 'projects:merge_requests'; // matches all `projects:merge_request.* pages
    const locationSelector = 'div[class="file-header-content"]'; // your selector where to inject your component
    let container = function () { return CreateContainer(appName, extensionId) }; // boiler, but needed copy this to your feature as well
    InjectComponent(pageType, locationSelector, container, appJS);
}
```

5. Now we need to update the `./src/content-scripts/content-script.js` to actually load our content injection function:

```javascript
...
// Add your injections here
MRDiff(extensionId);
```

6. That should be it, try reloading the plugin, putting in your OpenAI key, saving and give the feature a try.

### Troubleshooting

Debugging chrome extensions is not straight forward, there are three contexts, all of which are different and have to be debugged individually. Debugging communication between the three can be troublesome. Many times when adding new features you have to forcibly reload the plugin. In some rare cases you have to uninstall the plugin and reinstall it. 

The contexts are:
1. The content-script context: This is running inside of gitlab.com and is restricted to the same restrictions any gitlab html is, meaning the content-security-policy may block features.
2. The background context: This is handling calls to open ai and giving settings access to the content-scripts
3. The settings panel context: This handles saving settings and communicates it back to the background context.

To debug content-script context, simply open devtools for the page you are on.

To debug the background context, open the `chrome://extensions` page, find the `inspect views service worker (Inactive)` link and click it to open up a new devtools window.

To debug the settings panel context, open the settings pane by clicking the extension GitLab Tanuki icon, once the settings page is visible right click `Inspect` to open a new devtools window.

### MRDiff Example View:

![](docs/explain_button.png){width=50%}

![](docs/explain_modal.png){width=50%}

### Prompt Collection Overlay

Currently the overlay is a WIP.   

