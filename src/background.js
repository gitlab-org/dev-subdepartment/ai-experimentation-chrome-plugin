import Api from "@/lib/api";
import ExpiryMap from "expiry-map";
import {getUserSettings} from './utils/commonUtils'

let messageCache = new ExpiryMap(10000, []);
console.log('hello api')
let api = new Api();

// incoming messages from extension based code
chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {

  if (sender.origin === 'https://gitlab.com' || (chrome.runtime.id === sender.id)) {
    handleMessage(message, sendResponse);
    return true;
  } else {
    console.error('ignoring message from non-gitlab sender: %o', sender)
    return false;
  }
});

// incoming messages from overlay/injected content-script code
chrome.runtime.onMessageExternal.addListener(function (message, sender, sendResponse) {
  if (sender.origin === 'https://gitlab.com') {
    handleMessage(message, sendResponse);
    return true;
  } else {
    console.error('ignoring content-script message from non-gitlab sender: %o', sender)
    return false;
  }
});

// Assumes message sender origin has been validated
function handleMessage(message, sendResponse) {
  switch (message.type) {
    case 'reload_settings':
      console.log('background: reload_settings: settings reloading: %o');
      api.loadSettings();
      sendResponse(true);
      break;
    case 'bglog':
      console.log(message.obj);
      break;
    case 'get_settings':
      getUserSettings().then(({ session, persisted }) =>
        sendResponse({
          session,
          persisted,
        })
      );
      break;
    case 'get_available_models':
      api.fetchModels().then((result) => {
        sendResponse(result);
      })
      break;
    case 'prompt_request':
      console.log('executing prompt_request with', message.prompt, message.settings )
      console.log('prompt_request: api settings', api.settings)
      api.sendPromptRequest(message.prompt, message.settings)
        .then(response => response.json())
        .then(data => {
          console.log('received prompt_request data:', data);
          sendResponse({ response: data });
          console.log('got json data response: %o', data);
          return data;
        })
        .catch(error => {
          console.log('error prompt_request:', error);
          sendResponse({ response: '', error });
        });
      break;
    case 'request':
      var response = messageCache.get(message.url);
      if (response) {
        sendResponse({'response': response});
      }

      fetch(message.url, message.options).then((response) => {
        return response.text().then(data => {
          messageCache.set(message.url, data);
          sendResponse({'response': data});
          return data;
        });
      }).catch(e => {
        sendResponse({'response': {}, 'error': e});
      });
    
      break;
  }
}
