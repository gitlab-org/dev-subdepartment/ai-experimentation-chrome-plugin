import Vue from 'vue';
import App from './App.vue';

import setConfigs from '@gitlab/ui/dist/config';

setConfigs();

const appName = 'gl-ai-extension-overlay-app';

/* eslint-disable no-new */
new Vue({
  el: '#'+appName+'',
  appName: appName,
  render: (h) => h(App),
});
