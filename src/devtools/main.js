import Vue from 'vue';
import App from './App.vue';

import setConfigs from '@gitlab/ui/dist/config';

setConfigs();

window.bglog = function (obj) {
  if (chrome && chrome.runtime) {
    chrome.runtime.sendMessage('ikednjolljfjpgeahfmkedkjhgpgodpl', { type: 'bglog', obj: obj });
  }
};

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: (h) => h(App),
});
