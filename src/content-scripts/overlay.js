import { CreateContainer, InjectScripts } from './injector';

const overlayAppName = 'gl-ai-extension-overlay-app';

var showOverlay = false;

function injectButton(extensionId) {
    let target = document.getElementsByClassName("header-user")[0];
    if (!target) {
        return;
    }

    let aiButton = document.createElement('button');
    aiButton.setAttribute('id', 'ai-button')
    aiButton.setAttribute('extensionId', extensionId)
    aiButton.addEventListener('click', function() {
        showOverlay = !showOverlay;
        if (showOverlay) {
            injectOverlay(extensionId);
            aiButton.innerText = 'Hide AI Assist';
        } else {
            deleteOverlay(extensionId);
            aiButton.innerText = 'Show AI Assist';
        }
    })

    aiButton.setAttribute('class', 'gl-btn');
    aiButton.innerText = 'Show AI Assist';

    target.insertAdjacentElement('afterend', aiButton);
}

function deleteOverlay() {
    let child = document.getElementsByClassName(overlayAppName)[0];
    if (child) {
        child.parentElement.removeChild(child);
    }
}

// inject ourselves underneath the header by creating a div and using 
// chrome.runtime.getURL(...) to load the necessary compiled Vue components.
function injectOverlay(extensionId) {
    // Inject overlay
    let target = document.getElementsByTagName("header")[0];
    if (!target) {
        return;
    }
 
    const container = CreateContainer(overlayAppName, extensionId);

    
    container.style.position = 'fixed';
    container.style.top = '50%';
    container.style.left = '50%';
    container.style.transform = 'translate(-50%, -50%)';
    container.style.width = '80%';
    container.style.height = '80%';
    container.style.maxWidth = '1000px';
    container.style.maxHeight = '1000px';
    // forcibly set otherwise it will be transparent
    let currentBackground = window.getComputedStyle(target).backgroundColor;
    container.style.backgroundColor = currentBackground;
    //container.style.backgroundColor = '#fff';
    container.style.zIndex = '10000';
    container.style.borderRadius = '10px';
    container.style.boxShadow = '0 0 10px rgba(0, 0, 0, 0.5)';
    container.style.overflow = 'auto';

    const overlayApp = document.createElement('div');
    overlayApp.setAttribute('id', overlayAppName);
    container.appendChild(overlayApp)

    target.insertAdjacentElement('afterend', container);
    // inject only after element is ready
    InjectScripts(['/js/overlay.js']);
}

export function CreateOverlay(extensionId) {
    chrome.runtime.sendMessage(extensionId, {type: 'get_settings'}, function (response) {
        console.log('content script checking settings: %o', response);
       
        if (response?.persisted?.show_assist_button) {
            injectButton(extensionId);
        }
    });
}
