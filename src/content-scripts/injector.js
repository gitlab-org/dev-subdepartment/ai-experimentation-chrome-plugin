
// Creates a container for a Vue view/Component.
export function CreateContainer(appName, extensionId) {
    const container = document.createElement('div');
    container.classList.add(appName);
    container.setAttribute('appName', appName);
    container.setAttribute('extensionId', extensionId);
    return container;
}

// InjectScripts removing stale script files if they already exist and reloading new ones
export function InjectScripts(scriptsToInject) {
    let body = document.getElementsByTagName("body")[0];
    var scriptContainer = document.getElementById('gl-extension-scripts');
    if (!scriptContainer) {
        scriptContainer = document.createElement('div');
        scriptContainer.setAttribute('id', 'gl-extension-scripts');
    }

    scriptsToInject.forEach((script) => {
        var scriptTag = document.getElementById('script_'+script);
        if (scriptTag) {
            //console.log('already imported, removing and reimporting');
            scriptContainer.removeChild(scriptTag);
        }
        
        //console.log('injecting script: %o', script);
        scriptTag = document.createElement('script'); 
        scriptTag.id = 'script_'+script;
        scriptTag.src = chrome.runtime.getURL(script); 
        scriptContainer.appendChild(scriptTag);
    })

    body.appendChild(scriptContainer);
}

// InjectComponent requires the 
// - gitlabPageType which comes from the body dataset.page value.
// - locationSelector a css selector to find child elements
// - appName name of the Vue app (set in the main.js Vue.el property)
// - containerName name of the container to create for the Vue app (set in the main.js Vue.containerName property)
// - scriptName the path to the vue App.vue: e.g. /mrdiff/main.js will be compiled to /js/mrdiff.js
export function InjectComponent(gitlabPageType, locationSelector, containerCreator, scriptName) {

    window.addEventListener('load', function () {
        // exit out if we are not on the correct page
        if (!isOnCorrectPage(gitlabPageType)) {
            console.log('not on correct page, not injecting.');
            return;
        }

        console.log('In correct page... continuing');
        // called for each element
        let callback = function () {
            // just do whole document searches for now.
            let locations = document.querySelectorAll(locationSelector)
            locations.forEach((e) => {
                // tracks if we have injected before
                if (e.hasAttribute('gl-ext-injected')) {
                    return;
                }
               
                let container = containerCreator(); // CreateContainer('mrdiff', appName, extensionId);
                e.insertAdjacentElement('afterend', container);
                e.setAttribute('gl-ext-injected', true);
                // inject only after element is ready
                InjectScripts([scriptName]);
            })
        }

        listenPageModifications(callback);
    });
}

// Calls callback with every element found, provided we are on the correct page.
// This allows more customization on how / when to search for elements
export function FindElementsWithCallback(pageType, callback) {
    window.addEventListener('load', function () {
        // exit out if we are not on the correct page
        if (isOnCorrectPage(pageType)) {
            return;
        }
        listenPageModifications(callback);
    });
}

// listens for page modifications to call the callback if a valid element was found
function listenPageModifications(cb) {
    const targetNode = document.getElementsByTagName("body")[0];
    const config = { attributes: false, childList: true, subtree: true };

    const mutationCallback = (mutationList) => {
        for (const mutation of mutationList) {

            if (mutation.type === "childList" && mutation.addedNodes.length > 0) {
                mutation.addedNodes.forEach((ele) => {
                    if (ele.nodeType !== 1) {
                        return;
                    }

                    cb(ele);
                });
            } 
        }
    };

    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(mutationCallback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, config);
}

function isOnCorrectPage(pageType) {
    const body = document.getElementsByTagName('body')[0];
    if (!body || !body.dataset) {
        console.log('body or dataset was undefined');
        return false;
    }
    const dataset = body.dataset;

    if (typeof dataset.page === 'string' && dataset.page.startsWith(pageType)) {
        return true;
    }
    return false;
}