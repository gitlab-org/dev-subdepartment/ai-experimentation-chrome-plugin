import models from '../popup/models.json';

// loads models from the cached data
export function getModels(searchText = '') {
  return models.data
    .filter(({id}) => id.toLowerCase().includes(searchText.toLowerCase())) // filter models by searchText
    .map(({id}) => ({text: id, value: id})) || [];
}

// sends prompt request to open ai 
export async function sendPrompt(extensionId, prompt, settings) {
  return sendMessage(extensionId, {type: 'prompt_request', prompt, settings });
}

// retrieves settings from session + local storage
export async function getSettings(extensionId) {
  return sendMessage(extensionId, {type: 'get_settings'});
}

async function sendMessage(extensionId, options) {
  return new Promise((resolve, reject) => {
    chrome.runtime.sendMessage(extensionId, options, (response) => {
      if (chrome.runtime.lastError) {
        reject(chrome.runtime.lastError);
      } else {
        resolve(response);
      }
    });
  });
}

export const defaultPersistedSettings = (settings) => {
  return {
    model: settings?.model || 'text-davinci-003',
    show_assist_button: settings?.show_assist_button || false,
    extra_config: settings?.extra_config || "",
    top_p: settings?.top_p || "1",
    temperature: settings?.temperature || "1",
    max_tokens: settings?.max_tokens || "256",
  };
};

export const defaultSessionSettings = (settings) => {
  return {
    key: settings?.key || "",
  };
};

export const getUserSettings = async () => {
  const [session, persisted] = await Promise.all([
    new Promise((resolve) => {
      chrome.storage.session.get(['session_settings'], (data) => {
        resolve(data?.session_settings);
      });
    }),
    new Promise((resolve) => {
      chrome.storage.local.get(['persisted_settings'], (data) => {
        resolve(data?.persisted_settings);
      });
    }),
  ]);
  console.log('getUserSettings:', session, persisted)
  return {session, persisted}
};
