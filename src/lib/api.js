import { getUserSettings } from '../utils/commonUtils'

const API_HOST = "https://api.openai.com"
export default class Api {
    settings = {};
  
    constructor() {
      this.loadSettings();
    }

    getHeaders() {
      console.log("api get headers: ", this.settings, this.settings.key)
      return {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.settings?.key,
      }
    }

    async loadSettings() {
      const {persisted, session} = await getUserSettings();
      console.log('api: settings loaded', persisted, session)
      this.settings = {...persisted, ...session};
    }

    // this is temporarily not used
    async fetchModels() {
      let defaultModels = [{ text: "text-davinci-003", value: "text-davinci-003" }];
  
      if (!this.settings?.key) {
        return defaultModels;
      }
  
      try {
        const response = await fetch(`${API_HOST}/v1/models`, {
          headers: this.getHeaders(),
        });
  
        const data = await response.json();
  
        if (!data?.data) {
          console.log(
            "failed to get latest list of models defaulting to text-davinci-003"
          );
          return defaultModels;
        }
  
        return data.data.map((model) => ({ text: model.id, value: model.id }));
      } catch (e) {
        console.log("failed to load %o", e);
      }
  
      return defaultModels;
    }
  
    async sendPromptRequest(parsedPrompt, customSettings) {
      return fetch(`${API_HOST}/v1/completions`, {
        method: "POST",
        headers: this.getHeaders(),
        body: JSON.stringify({
          model: this.settings.model,
          prompt: parsedPrompt,
          temperature: parseFloat(this.settings.temperature),
          max_tokens: parseInt(this.settings.max_tokens),
          top_p: parseFloat(this.settings.top_p),
          frequency_penalty: 0.0,
          presence_penalty: 0.0,
          stop: ["\"\"\""],
          ...JSON.parse(this.settings.extra_config || '{}'),
          ...customSettings,
        }),
      });
    }
  }