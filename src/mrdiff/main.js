import Vue from 'vue';
import App from './App.vue';

import setConfigs from '@gitlab/ui/dist/config';

setConfigs();

const appName = 'gl-ai-extension-mrdiff-app';

/* eslint-disable no-new */
new Vue({
  el: 'div[class="'+appName+'"]',
  appName: appName,
  render: (h) => h(App),
});
